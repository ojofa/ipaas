# iPaaS

### iPaas: Integration Platform as a Service

**iPaaS** is an integration and automation tool. As of August, 2023, Solution Delivery is exploring the viability of iPaaS
as a low-code no-code option to delivering solutions.

## Getting Started

* ### Account Registration and Login  
    - [ ] [Sign up for a TeamDynamix iPaaS user account - Make a request]()
    - [ ] [Sign up to the TeamDynamix iPaaS Knowledge Base (KB)](https://community.teamdynamix.com/RegisterAccount.aspx)
    - [ ] [Login to the iPaaS User Interface](https://us.ipaas.teamdynamix.com/account/login)   

* ### Training materials  
    - [ ] [Canvas - iPaaS Video Training](https://miamioh.instructure.com/courses/204577)
    - [ ] [TeamDynamix iPaaS Knowledge Base (KB)](https://solutions.teamdynamix.com/TDClient/1965/Portal/KB/?CategoryID=19010)
      - [Introduction to iPaaS](https://solutions.teamdynamix.com/TDClient/1965/Portal/KB/?CategoryID=19660)
      - [Working with the Flow Designer](Working with the Flow Designer)
      - [Working with Flows](https://solutions.teamdynamix.com/TDClient/1965/Portal/KB/?CategoryID=19652)
      - [Working with Connectors](https://solutions.teamdynamix.com/TDClient/1965/Portal/KB/?CategoryID=21530)
      - [Dynamic Forms](https://solutions.teamdynamix.com/TDClient/1965/Portal/KB/?CategoryID=22450)
      - [E-Learning Training Videos: iPaaS Foundations](https://solutions.teamdynamix.com/TDClient/1965/Portal/KB/?CategoryID=22837)    

* ### Getting help  
    - **Solution Delivery Communication Channels**
      - Slack: [#iPaaS](https://join.slack.com/share/enQtNTcwNjI1ODA0MDgxNy02MmJmYWIxZTA2NTZjYTZlZDNhNTE2YzlmNjg3M2Q5OGUwYjE2MzhlMTUxYmE0MzUzOTVlMzg2YTYwNTEwYzBh)
    - **TeamDynamix Support**
      - [Submit a TeamDynamix Support ticket](https://solutions.teamdynamix.com/TDClient/1965/Portal/Requests/ServiceDet?ID=2147)
      - [Ask a question](https://solutions.teamdynamix.com/TDClient/1965/Portal/Questions)
    - **Join a TeamDynamix community**
      - [Find and join a group within the TeamDynamix community](https://community.teamdynamix.com/)  
    - **Join a TeamDynamix Slack channel
      - [Join/Find Slack channels](https://join.slack.com/t/tdxflowgrammers/shared_invite/zt-1yz424jdj-9aZo2T_7xkb5IDuALT7WcQ)